import React from "react";
import {Media} from "react-bootstrap";
import "./result-item.css";
import {Link} from "react-router-dom";

export default function ResultItem(props) {
    const {track} = props;
    const artists = track.artists ? track.artists?.map(artist => artist.name).join() : 'MyTone';

    return (
        <div className="result-item d-flex align-items-center justify-content-between px-3 py-1 my-3">
            <Media className="media-item d-flex align-items-start justify-content-center">
                {!props.album && (
                    <img
                        height="40px"
                        width="40px"
                        className="mr-3"
                        src={track.image?.url}
                        alt="Album Cover"
                    />)}
                <Media.Body>
                    <div className="track-info font-weight-bold">
                        <div className="d-flex align-items-center justify-content-center">
                            <div className="trackIndex mr-4">
                                <Link to={`/player/${track.id}`} className="text-white">
                                    <i className="fas fa-play"/>
                                </Link>
                            </div>
                            <div className="d-flex flex-column align-items-start justify-content-center">
                                <div>{track.name}</div>
                                <div className="text-muted">{artists} - {track.duration}</div>
                            </div>
                        </div>
                    </div>
                </Media.Body>
            </Media>
        </div>
    );
}
