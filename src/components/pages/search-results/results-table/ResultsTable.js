import React, {useEffect} from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import uuid from "uuid";
import ResultItem from "./result-item/ResultItem";
import Loader from "../../../UI/loader/Loader";

export default function ResultsTable(props) {
    useEffect(() => {
        loadItems();
    }, [props.search.url]);

    const loadItems = () => {
        props.search.onSearch(
            props.search.url,
            props.search.offset,
            props.search.items
        );
    };

    return (
        <InfiniteScroll
            dataLength={props.search.items.length}
            next={loadItems}
            hasMore={props.search.hasMoreItems}
            loader={<Loader size="md"/>}
        >
            {props.search.items.map(item => (
                <ResultItem key={uuid.v4()} track={item}/>
            ))}
        </InfiniteScroll>
    );
}
