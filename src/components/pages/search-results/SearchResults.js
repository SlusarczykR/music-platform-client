import React, {useEffect, useState} from "react";
import {TRACK_SEARCH} from "../../../endpoints";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./search-results.css";
import ResultsTable from "./results-table/ResultsTable";
import * as actions from "../../../store/actions/index";

function SearchResults(props) {
    const input = props.match.params.input;
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
        props.onInitSearch(TRACK_SEARCH + `/${input}`);
        return () => {
            setMounted(false);
        };
    }, [input]);

    return (
        mounted && (
            <section className="search-results">
                <div className="container">
                    <div className="search-results-c-container">
                        <h3 className="search-results-header font-weight-bold text-white">Tracks</h3>
                        <ResultsTable
                            search={{
                                url: props.url,
                                items: props.items,
                                offset: props.offset,
                                hasMoreItems: props.hasMoreItems,
                                onSearch: props.onSearch,
                                onInitSearch: props.onInitSearch
                            }}
                        />
                    </div>
                </div>
            </section>
        )
    );
}

const mapStateToProps = state => {
    return {
        url: state.search.url,
        items: state.search.items,
        offset: state.search.offset,
        hasMoreItems: state.search.hasMoreItems
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitSearch: url => dispatch(actions.initSearch(url)),
        onSearch: (url, offset, items) =>
            dispatch(actions.search(url, offset, items))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(SearchResults)
);
