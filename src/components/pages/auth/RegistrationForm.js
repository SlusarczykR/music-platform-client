import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Alert, Button, Form} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import * as actions from "../../../store/actions/index";
import InputUtils from "../../../utils/form/InputUtils";
import CustomInput from "../../UI/form/input/CustomInput";
import Loader from "../../UI/loader/Loader";

function RegistrationForm(props) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [alert, setAlert] = useState(null);
    const [usernameInputValidity, setUsernameInputValidity] = useState(true);
    const [passwordInputValidity, setPasswordInputValidity] = useState(true);
    const [inputsTouched, setInputsTouched] = useState(false);

    useEffect(() => {
        return () => {
            props.onCleanupUser();
        }
    }, []);

    useEffect(() => {
        setInputsTouched(true);
    }, [username, password]);

    useEffect(() => {
        if (props.error) {
            setAlert(props.error);
        } else if (!props.error && props.newUser) {
            setAlert("User has been successfully created");
        }
    }, [props.error, props.newUser]);

    const handleSubmit = e => {
        e.preventDefault();
        if (InputUtils.checkValidity(username, {required: true, minLength: 3, maxLength: 20})) {
            setUsernameInputValidity(true);
            if (InputUtils.checkValidity(password, {required: true, minLength: 8, maxLength: 20})) {
                setPasswordInputValidity(true);
                props.onAddUser(username, password);
            } else {
                setPasswordInputValidity(false);
            }
        } else {
            setUsernameInputValidity(false);
        }
    };

    const submitBtn = !props.isLoading ? (
        <Button variant="warning" type="submit" block>
            Create Account
        </Button>
    ) : (
        (<Loader size="sm"/>)
    );

    const errorMessage = (
        <Alert variant="danger" onClose={() => setAlert(null)} dismissible>
            <p>{alert}</p>
        </Alert>
    );

    const successMessage = (
        <Alert variant="success" onClose={() => setAlert(null)} dismissible>
            <p>{alert}</p>
        </Alert>
    );

    const alertMessage = alert && (alert === "User has been successfully created" ? successMessage : errorMessage);

    return (
        <>
            {alertMessage}
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label className="font-weight-bold">Username*</Form.Label>
                    <CustomInput
                        type="text"
                        placeholder="Username"
                        changeHandler={setUsername}
                        required={true}
                        alertMessage="Username must be between 3 and 20 characters!"
                        invalid={!usernameInputValidity}
                        touched={inputsTouched}
                    />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label className="font-weight-bold">Password*</Form.Label>
                    <CustomInput
                        type="password"
                        placeholder="Password"
                        changeHandler={setPassword}
                        required={true}
                        alertMessage="Password must be between 8 and 20 characters!"
                        invalid={!passwordInputValidity}
                        touched={inputsTouched}
                    />
                </Form.Group>
                {submitBtn}
            </Form>
        </>
    );
}

const mapStateToProps = state => {
    return {
        newUser: state.user.username,
        isLoading: state.user.loading,
        error: state.user.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddUser: (username, password) =>
            dispatch(actions.addUser(username, password)),
        onCleanupUser: () =>
            dispatch(actions.cleanupUser())
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(RegistrationForm)
);
