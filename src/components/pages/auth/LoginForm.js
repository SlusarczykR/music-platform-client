import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Alert, Button, Form} from "react-bootstrap";
import {withRouter} from "react-router-dom";
import * as actions from "../../../store/actions/index";
import InputUtils from "../../../utils/form/InputUtils";
import CustomInput from "../../UI/form/input/CustomInput";
import Loader from "../../UI/loader/Loader";

function LoginForm(props) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [alert, setAlert] = useState(null);
    const [usernameInputValidity, setUsernameInputValidity] = useState(true);
    const [passwordInputValidity, setPasswordInputValidity] = useState(true);
    const [inputsTouched, setInputsTouched] = useState(false);

    useEffect(() => {
        return () => {
            props.onCleanupAuth();
        }
    }, []);

    useEffect(() => {
        setInputsTouched(true);
    }, [username, password]);

    useEffect(() => {
        if (props.error) {
            setAlert(props.error);
        }
    }, [props.error]);

    const handleSubmit = e => {
        e.preventDefault();
        if (InputUtils.checkValidity(username, {required: true, minLength: 3, maxLength: 20})) {
            setUsernameInputValidity(true);

            if (InputUtils.checkValidity(password, {required: true, minLength: 8, maxLength: 20})) {
                setPasswordInputValidity(true);
                props.onAuth(username, password);
            } else {
                setPasswordInputValidity(false);
            }
        } else {
            setUsernameInputValidity(false);
        }
    };

    const submitBtn = !props.isLoading ? (
        <Button className="c-btn-main" type="submit" block>
            Log in
        </Button>
    ) : (<Loader size="sm"/>);

    const errorMessage = (
        <Alert variant="danger" onClose={() => setAlert(null)} dismissible>
            <p>{alert}</p>
        </Alert>
    );

    const alertMessage = alert && errorMessage;

    return (
        <>
            {alertMessage}
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label className="font-weight-bold">Username</Form.Label>
                    <CustomInput
                        type="text"
                        placeholder="Username"
                        changeHandler={setUsername}
                        required={true}
                        alertMessage="Username must be between 3 and 20 characters!"
                        invalid={!usernameInputValidity}
                        touched={inputsTouched}
                    />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label className="font-weight-bold">Password</Form.Label>
                    <CustomInput
                        type="password"
                        placeholder="Password"
                        changeHandler={setPassword}
                        required={true}
                        alertMessage="Password must be between 8 and 20 characters!"
                        invalid={!passwordInputValidity}
                        touched={inputsTouched}
                    />
                </Form.Group>
                {submitBtn}
            </Form>
        </>
    );
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null,
        isLoading: state.auth.loading,
        error: state.auth.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(actions.auth(username, password)),
        onCleanupAuth: () =>
            dispatch(actions.cleanupAuth())
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(LoginForm)
);
