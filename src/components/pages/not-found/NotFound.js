import React from "react";
import {Container, Image} from "react-bootstrap";
import notFoundGif from "../../../assets/gif/not-found-ufo.gif";
import {Link} from "react-router-dom";
import "./not-found.css";

export default function NotFound() {
    return (
        <section id="not-found" className="my-5">
            <Container>
                <div id="not-found-wrapper" className="d-flex flex-column justify-content-center align-items-center">
                    <div id="not-found-img">
                        <Image src={notFoundGif} alt="NotFoundGif" fluid/>
                    </div>
                    <div id="header" className="text-center">
                        <h1 className="display-3 font-weight-bold">404</h1>
                        <h2 id="sub-header" className="mb-4">Page not found</h2>
                        <div id="info-message">
                            The Page you are looking for doesn't exist or another error
                            occurred.<br/>
                            <Link to={`/`} className="text-main">
                                Go back to home page.
                            </Link>
                        </div>
                    </div>
                </div>
            </Container>
        </section>
    );
}