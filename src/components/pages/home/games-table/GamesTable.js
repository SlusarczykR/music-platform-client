import React, {useEffect, useState} from "react";
import {
  RECENTLY_RELEASED_GAMES,
  MOST_POPULAR_GAMES,
  COMING_SOON_GAMES
} from "../../../../endpoints";
import TableCol from "./table-col/TableCol";
import uuid from "uuid";
import { Container, Row, Col } from "react-bootstrap";
import "./games-table.css";

export default function GamesTable() {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
    return () => {
      setMounted(false);
    };
  }, []);

  return mounted &&  (
    <section id="tables">
      <Row>
        <Col sm={12} md={4}>
          <TableCol
            key={uuid.v4()}
            title="Recently released"
            link="recentlyReleased"
            url={RECENTLY_RELEASED_GAMES}
            type="releaseDate"
          />
        </Col>
        <Col sm={12} md={4}>
          <TableCol
            key={uuid.v4()}
            title="Coming soon"
            link="comingSoon"
            url={COMING_SOON_GAMES}
            type="releaseDate"
          />
        </Col>
        <Col sm={12} md={4}>
          <TableCol
            key={uuid.v4()}
            title="Most anticipated"
            link="mostAnticipated"
            url={MOST_POPULAR_GAMES}
            type="game"
          />
        </Col>
      </Row>
    </section>
  );
}
