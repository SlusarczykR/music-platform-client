import React from "react";
import {useHttp} from "../../../../../hooks/http";
import TableItem from "./table-item/TableItem";
import uuid from "uuid";
import "./table-col.css";
import {Link} from "react-router-dom";
import Loader from "../../../../UI/loader/Loader";

export default function TableCol(props) {
    const [isLoading, fetchedData] = useHttp(props.url, []);
    const data = fetchedData ? fetchedData.data : [];

    return (
        <div className="c-t-col">
            <div className="header-content">
                <h1 className={"col-title"}>
                    <Link to={`/games/${props.link}`}>
                        {props.title}
                    </Link>
                </h1>
            </div>
            <div className="bottom-line"></div>
            {isLoading ? (<Loader size="sm"/>) : (
                data.map(item => (
                    <TableItem key={uuid.v4()} item={item} type={props.type}/>
                ))
            )}
        </div>
    );
}
