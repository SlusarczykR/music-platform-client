import React from "react";
import noImage from "../../../../../assets/img/no-image.png";
import { withRouter } from "react-router-dom";
import { Card } from "react-bootstrap";
import "./article-item.css";

function ArticleItem(props) {
  const { game } = props;
  const gameId = game.id;
  const cover = game.cover && game.cover;
  const coverUrl = cover && cover.url && cover.url != "" ? cover.url : noImage;

  const getDetails = () => {
    props.history.push("/games/" + gameId);
  };

  return (
    <Card onClick={getDetails} className="text-white">
      <Card.Img variant="top" src={coverUrl} />
      <Card.ImgOverlay className="d-flex align-items-end justify-content-start">
        <Card.Title>{game.name}</Card.Title>
      </Card.ImgOverlay>
    </Card>
  );
}

export default withRouter(ArticleItem);
