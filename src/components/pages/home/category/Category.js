import Header from "../../../UI/header/Header";
import GameCarousel from "../../../UI/game-carousel/GameCarousel";
import {CATEGORIES} from "../../../../endpoints";
import React from "react";

export default function Category({id, name, responsive}) {
    return (
        <>
            <Header className="header-wrapper" title={name}/>
            <GameCarousel
                url={CATEGORIES + id + "/playlists"}
                slides={5}
                responsive={responsive}
                footer={true}
            />
        </>
    );
}