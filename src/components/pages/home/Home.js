import React, {useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import {Container} from "react-bootstrap";
import {NEW_RELEASES, RECENT_CATEGORIES} from "../../../endpoints";
import Header from "../../UI/header/Header";
import GameCarousel from "../../UI/game-carousel/GameCarousel";
import "./home.css";
import {useHttp} from "../../../hooks/http";
import uuid from "uuid";
import Category from "./category/Category";

function Home() {
    const [mounted, setMounted] = useState(false);
    const [isLoading, fetchedData] = useHttp(RECENT_CATEGORIES, []);
    const categories = fetchedData ? fetchedData.data : [];

    useEffect(() => {
        setMounted(true);
        return () => {
            setMounted(false);
        };
    }, []);

    const responsive = [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
            },
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
            },
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            },
        },
    ];

    const newReleases = (
        <>
            <Header className="header-wrapper" title="New Releases"/>
            <GameCarousel
                url={NEW_RELEASES}
                slides={5}
                responsive={responsive}
                footer={true}
            />
        </>
    );

    return mounted && (
        <section id="home-wrapper">
            <Container>
                {isLoading ? (
                    <div className="hallow-loader">
                        <div className="p1"/>
                    </div>
                ) : (
                    <>
                        {newReleases}
                        {categories.map(category => (
                            <Category key={uuid.v4()} id={category.id} name={category.name} responsive={responsive}/>
                        ))}
                    </>
                )}
            </Container>
        </section>
    );
}

export default withRouter(Home);
