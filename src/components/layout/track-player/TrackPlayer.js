import React, {useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import Loader from "../../UI/loader/Loader";
import SpotifyPlayer from "react-spotify-web-playback";
import {TOKEN} from "../../../endpoints";
import {useHttp} from "../../../hooks/http";
import * as actions from "../../../store/actions";
import {connect} from "react-redux";
import "./track-player.css";

function TrackPlayer(props) {
    const {track, playlist} = props;
    const [mounted, setMounted] = useState(false);
    const [play, setPlay] = useState(false);
    const [isLoading, fetchedData] = useHttp(TOKEN, [track]);
    const token = fetchedData ? fetchedData.data : "";
    const trackSet = track ? getTrackSet() : [];

    function getTrackSet() {
        if (playlist) {
            const currentTrackIndex = playlist.indexOf(track);
            const currentPlaylist = playlist.slice(currentTrackIndex, playlist.length);
            return currentPlaylist?.map(song => song.uri);
        }
        return [track.uri];
    }

    useEffect(() => {
        setMounted(true);
        setPlay(true);
        return () => {
            setMounted(false);
        };
    }, [track]);

    function playerCallback(state) {
        console.log('# Player state:');
        console.log(state.track);
        console.log(track);
        if (!state.isPlaying) {
            setPlay(false);
        }

        if ((state.track.id !== "") && (state.track.id !== track.id)) {
            props.onChangePlayingTrack(findTrack(state.track.id));
        }
    }

    function findTrack(id) {
        console.log('$$ findTrack: ' + id);
        console.log(playlist);
        return playlist.find(t => t.id === id);
    }

    const styles = {
        activeColor: '#37FDFC',
        bgColor: '#282828',
        altColor: '#37FDFC',
        color: '#fff',
        loaderColor: '#37FDFC',
        sliderColor: '#37FDFC',
        trackArtistColor: '#fff',
        trackNameColor: '#fff',
    };

    return track != null ? (!token ? (
        <Loader size="md"/>
    ) : (
        <div id="track-player" className="footer fixed-bottom">
            <SpotifyPlayer
                callback={playerCallback}
                token={token}
                uris={trackSet}
                styles={styles}
                play={play}
                autoPlay={true}
            />
        </div>
    )) : <></>;
}

const mapStateToProps = state => {
    return {
        track: state.player.track,
        playlist: state.player.playlist
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onPlayTrack: (player) => dispatch(actions.play(player)),
        onChangePlayingTrack: (player) => dispatch(actions.changePlayingTrack(player))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(TrackPlayer)
);
