import React, {useState} from "react";
import {DelayInput} from "react-delay-input";
import {NavLink, withRouter} from "react-router-dom";
import {Button, Container, Form, InputGroup, Nav, Navbar, NavDropdown,} from "react-bootstrap";
import "./custom-navbar.css";

const CustomNavbar = (props) => {
    const [value, setValue] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        value !== "" && props.history.push("/search/" + value);
        setValue("");
    };

    const logout = () => {
        console.log('** logout');
        props.player.onPlayTrack({track: null, playlist: []});
        props.auth.onLogout();
        props.history.push("/");
    };

    const authBtn = props.auth.isAuth ? (
        <NavDropdown
            title={props.auth.username}
            className="auth-nav-dropdown main-nav-link"
        >
            <NavDropdown.Item>Profile</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={`/lists/${props.auth.username}`}>
                Lists
            </NavDropdown.Item>
            <NavDropdown.Divider/>
            <NavDropdown.Item onClick={logout}>Log out</NavDropdown.Item>
        </NavDropdown>
    ) : (
        <>
            <Nav.Link
                className="main-nav-link text-main"
                onClick={() => props.onShowModal(1)}
            >
                Log in
            </Nav.Link>
            <Nav.Link className="main-nav-link" onClick={() => props.onShowModal(2)}>
                Register
            </Nav.Link>
        </>
    );

    const searchGamesForm = (
        <Form onSubmit={(event) => handleSubmit(event)} className="mr-3" inline>
            <InputGroup>
                <DelayInput
                    type="text"
                    value={value}
                    placeholder="Search"
                    className="form-control searchInput"
                    onChange={(event) => setValue(event.target.value)}
                />
                <InputGroup.Append>
                    <Button
                        className="text-center"
                        type="submit"
                        variant="outline-secondary"
                    >
                        <i className="fas fa-search"></i>
                    </Button>
                </InputGroup.Append>
            </InputGroup>
        </Form>
    );

    return (
        <Navbar expand="md" variant="dark">
            <Container>
                <Navbar.Brand as={NavLink} to="/" className="main-nav-link px-2">
                    MyTone
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    {searchGamesForm}
                    <Nav className="mr-auto">
                        <Nav.Link as={NavLink} exact to="/" className="main-nav-link">
                            Home
                        </Nav.Link>
                        <Nav.Link as={NavLink} to="/latestNews" className="main-nav-link">
                            Discover
                        </Nav.Link>
                        <Nav.Link as={NavLink} to="/latestNews" className="main-nav-link">
                            Browse
                        </Nav.Link>
                    </Nav>
                    <Nav className="ml-auto">{authBtn}</Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default withRouter(CustomNavbar);
