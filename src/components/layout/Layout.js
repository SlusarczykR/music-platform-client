import React, {useEffect} from "react";
import CustomNavbar from "./navbar/CustomNavbar";
import * as actions from "../../store/actions/index";
import Auth from "../pages/auth/LoginForm";
import CustomModal from "../UI/modal/CustomModal";
import RegistrationForm from "../pages/auth/RegistrationForm";
import TrackPlayer from "./track-player/TrackPlayer";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

function Layout(props) {
    useEffect(() => {
        console.log("modalHide() => Auth: isAuth");
        if (props.isAuth) props.onHideModal();
    }, [props.isAuth]);

    useEffect(() => {
        console.log("modalHide() => Auth: history.location.key");
        props.onHideModal();
    }, [props.history.location.key]);

    let modal;
    let modalTitle;

    console.log("@ChildId " + props.childId);

    switch (props.childId) {
        case 1:
            modal = <Auth/>;
            modalTitle = "Sign in";
            break;
        case 2:
            modal = <RegistrationForm/>;
            modalTitle = "Registration";
            break;
        default:
            modal = <></>;
            modalTitle = "";
            break;
    }

    return (
        <>
            <CustomNavbar
                onShowModal={props.onShowModal}
                auth={{
                    isAuth: props.isAuth,
                    username: props.username,
                    onLogout: props.onLogout
                }}
                player={{
                    onPlayTrack: props.onPlayTrack
                }}
            />
            <CustomModal
                show={props.modalShow}
                onHide={props.onHideModal}
                title={modalTitle}
            >
                {modal}
            </CustomModal>
            <main>{props.children}</main>
            <TrackPlayer/>
        </>
    );
}

const mapStateToProps = (state) => {
        return {
            newUser: state.user.username,
            isAuth: state.auth.token !== null,
            username: state.auth.username,
            modalShow: state.modal.modalShow,
            childId: state.modal.childId
        };
    }
;

const mapDispatchToProps = (dispatch) => {
        return {
            onAuth: (username, password) => dispatch(actions.auth(username, password)),
            onLogout: () => dispatch(actions.authLogout()),
            onShowModal: (childId) => dispatch(actions.modalShow(childId)),
            onHideModal: () => dispatch(actions.modalHide()),
            onPlayTrack: (player) => dispatch(actions.play(player))
        };
    }
;

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Layout)
);
