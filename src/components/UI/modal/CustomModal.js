import React from "react";
import {Container, Modal} from "react-bootstrap";
import "./custom-modal.css";

export default function CustomModal(props) {
    return (
        <Modal {...props} aria-labelledby="contained-modal-title-vcenter">
            <Modal.Header closeButton>
                <Modal.Title
                    id="contained-modal-title-vcenter"
                    className="font-weight-bold"
                >
                    {props.title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>{props.children}</Container>
            </Modal.Body>
        </Modal>
    );
}
