import React, {useState} from "react";
import {useHttp} from "../../../hooks/http";
import uuid from "uuid";
import GameCarouselItem from "./game-carousel-item/GameCarouselItem";
import Slider from "react-slick";
import SliderArrow from "./slider-arrow/SliderArrow";
import "./game-carousel.css";
import Loader from "../loader/Loader";

export default function GameCarousel(props) {
    const [isLoading, fetchedData] = useHttp(props.url, []);
    const [settings, setSettings] = useState({
        infinite: false,
        speed: 500,
        slidesToShow: props.slides,
        slidesToScroll: props.slides,
        responsive: props.responsive,
        prevArrow: <SliderArrow direction="left" to="prev"/>,
        nextArrow: <SliderArrow direction="right" to="next"/>
    });
    const items = fetchedData ? fetchedData.data : [];

    return (
        <section className="game-carousel">
            {isLoading ? (
                <Loader size="md"/>
            ) : (
                <div className="multiple-items">
                    <Slider {...settings}>
                        {items.map(item => (
                            <GameCarouselItem
                                key={uuid.v4()}
                                item={item}
                                footer={props.footer}
                            />
                        ))}
                    </Slider>
                </div>
            )}
        </section>
    );
}
