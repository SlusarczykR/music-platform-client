import React from "react";
import {withRouter} from "react-router-dom";
import {Card} from "react-bootstrap";
import "./game-carousel-item.css";

function GameCarouselItem(props) {
    const {item} = props;
    const id = item.id;
    const name = item.name;
    const image = item.image.url;

    const getDetails = () => {
        props.history.push("/tracks/" + item.type + "/" + id);
    };

    return (
        <Card onClick={getDetails}>
            <Card.Img variant="top" src={image}/>
            {props.footer && (
                <Card.Footer>
                    <small className="footer-title">{name}</small>
                </Card.Footer>
            )}
        </Card>
    );
}

export default withRouter(GameCarouselItem);
