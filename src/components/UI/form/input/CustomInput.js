import React from "react";
import {Form} from "react-bootstrap";
import "./custom-input.css";

export default function CustomInput(props) {
    const inputClasses = [];
    let alertMessage = "";

    if (props.invalid && props.touched) {
        inputClasses.push("invalid-input");
        alertMessage = props.alertMessage;
    }

    let customInput;

    switch (props.type) {
        case "textarea":
            customInput = (
                <Form.Control
                    className={inputClasses}
                    as="textarea"
                    rows={props.rows}
                    onChange={e => props.changeHandler(e.target.value)}
                    required={props.required}
                    value={props.value}
                />
            );
            break;
        default:
            customInput = (
                <Form.Control
                    className={inputClasses}
                    type={props.type}
                    placeholder={props.placeholder}
                    onChange={e => props.changeHandler(e.target.value)}
                    required={props.required}
                />
            );
            break;
    }

    return (
        <>
            {customInput}
            <small className="alert-message text-danger">{alertMessage}</small>
        </>);
}