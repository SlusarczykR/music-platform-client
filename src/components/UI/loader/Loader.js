import React from "react";
import Spinner from "react-bootstrap/Spinner";
import "./loader.css";

const Loader = props => {
    return (<div className="spinner-wrapper">
        <Spinner animation="grow" size={props.size}/>
    </div>)
};

export default Loader;