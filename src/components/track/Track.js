import React, {useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import {RECOMMENDATIONS, TRACK} from "../../endpoints";
import {Container, Table} from "react-bootstrap";
import Header from "../UI/header/Header";
import Loader from "../UI/loader/Loader";
import axios from "axios";
import TrackItem from "../track-holder/track-item/TrackItem";
import {v4} from "uuid";
import * as actions from "../../store/actions";
import {connect} from "react-redux";
import "./track.css";

function Track(props) {
    const trackId = props.match.params.id;
    const [mounted, setMounted] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [trackData, setTrackData] = useState({track: {}, playlist: []});

    useEffect(() => {
        setMounted(true);
        fetchTrackData();
        return () => {
            setMounted(false);
        };
    }, [trackId]);

    useEffect(() => {
        setTrackData({...trackData, track: props.track});
    }, [props.track]);

    useEffect(() => {
        setTrackData({...trackData, track: props.playingTrack});
    }, [props.playingTrack]);

    function fetchTrackData() {
        setIsLoading(true);
        axios.get(TRACK + trackId)
            .then(res => {
                return res.data;
            })
            .then(track => {
                const artistsIds = track.artists?.map(artist => artist.id).join();
                axios.get(RECOMMENDATIONS + '?artists=' + artistsIds + '&tracks=' + track.id)
                    .then(res => {
                        const playlist = res.data;
                        props.onPlayTrack({track: track, playlist: [track].concat(playlist)});
                        setIsLoading(false);
                        setTrackData({track, playlist});
                    })
            })
            .catch(e => {
                console.error(e);
                setIsLoading(false);
            });
    }

    return mounted && (
        <Container className="coming-soon-wrapper my-5">
            {isLoading ? (
                <Loader size="md"/>
            ) : (
                <>
                    <Header className="header-wrapper" title={trackData.track.name}/>
                    <div id="track-wrapper" className="d-flex flex-column align-items-center justify-content-center">
                        <div id="track-content-wrapper"
                             className="w-100 mb-5">
                            <div>
                                <img id="track-cover" height="360" width="360"
                                     src={trackData.track.image?.url}
                                     alt="albumCover"/>
                            </div>
                            <div id="list-table-wrapper">
                                <Table className="media-table text-white" responsive>
                                    <tbody>
                                    {trackData.playlist?.map((rec, index) => (
                                        <TrackItem onItemClick={props.onChangePlaylistTrack} key={v4()}
                                                   index={index + 1} track={rec}
                                                   albumId={rec.albumId} album={false} trackContext={true}/>
                                    ))}
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </div>
                </>
            )}
        </Container>
    );
}

const mapStateToProps = state => {
    return {
        track: state.player.track,
        playingTrack: state.player.playingTrack,
        playlist: state.player.playlist
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onPlayTrack: (player) => dispatch(actions.play(player))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Track)
);
