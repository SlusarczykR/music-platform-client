import React from "react";
import uuid from "uuid";
import {Media} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import "./track-item.css";
import * as actions from "../../../store/actions";
import {connect} from "react-redux";

function TrackItem(props) {
    const {track, albumId} = props;
    const [showPlay, setShowPlay] = React.useState(false);
    const artists = track.artists ? track.artists?.map(artist => artist.name).join() : 'MyTone';

    function displayPlayBtn() {
        setShowPlay(true);
    }

    function hidePlayBtn() {
        setShowPlay(false);
    }

    const trackIndexItem = showPlay ?
        (
            <Link to={`/player/${track.id}`} className="text-white">
                <i className="fas fa-play"/>
            </Link>
        ) : props.index;

    return (
        <tr onClick={(e) => {
            e.preventDefault();
            props.onChangePlaylistTrack(track);
        }} key={uuid.v4()} onMouseEnter={displayPlayBtn} onMouseLeave={hidePlayBtn} className="track-item">
            <td className="item-td">
                <div className="d-flex align-items-center justify-content-between px-3 my-1">
                    <Media className="media-item d-flex align-items-start justify-content-center">
                        {!props.album && (
                            <img
                                height="40px"
                                width="40px"
                                className="mr-3"
                                src={track.image?.url}
                                alt="Album Cover"
                            />)}
                        <Media.Body>
                            <div className="track-info font-weight-bold">
                                <div className="d-flex align-items-center justify-content-center">
                                    {!props.trackContext && (
                                        <div className="trackIndex mr-4">{trackIndexItem}</div>
                                    )}
                                    <div className="d-flex flex-column align-items-start justify-content-center">
                                        <div>{track.name}</div>
                                        <div className="artist text-muted">{artists}</div>
                                    </div>
                                </div>
                            </div>
                        </Media.Body>
                    </Media>
                    <div className="text-muted">{track.duration}</div>
                </div>
            </td>
        </tr>
    );
}

const mapDispatchToProps = dispatch => {
    return {
        onChangePlaylistTrack: (track) => dispatch(actions.changeTrack(track))
    };
};

export default withRouter(
    connect(
        null,
        mapDispatchToProps
    )(TrackItem)
);