import React, {useEffect, useState} from "react";
import {useHttp} from "../../hooks/http";
import {ALBUM, PLAYLISTS} from "../../endpoints";
import {Container, Table} from "react-bootstrap";
import TrackItem from "./track-item/TrackItem";
import uuid from "uuid";
import {Link, withRouter} from "react-router-dom";
import Header from "../UI/header/Header";
import Loader from "../UI/loader/Loader";
import "./track-holder.css";
import * as actions from "../../store/actions";
import {connect} from "react-redux";

function TrackHolder(props) {
    const id = props.match.params.id;
    const holderType = props.match.params.type;
    const [mounted, setMounted] = useState(false);
    const [isLoading, fetchedData] = useHttp((holderType === "album" ? ALBUM : PLAYLISTS) + id, [id]);
    const trackHolder = fetchedData ? fetchedData.data : {};
    const type = trackHolder.type?.charAt(0).toUpperCase() + trackHolder.type?.slice(1);
    const artists = trackHolder.artists ? trackHolder.artists?.map(artist => artist.name).join() : 'MyTone';
    const duration = trackHolder.totalDuration;
    const totalTracks = (trackHolder.total_tracks === 1) ? trackHolder.total_tracks + " track" : trackHolder.total_tracks + " tracks";
    const infoFirstRow = getInfoFirstRow();
    const infoSecondRow = getInfoSecondRow();


    function getInfoFirstRow() {
        if (holderType === "album") {
            return type + '-' + artists + '-' + trackHolder.release_date;
        }
        return trackHolder.description;
    }

    function getInfoSecondRow() {
        if (holderType === "album") {
            return totalTracks + '-' + duration;
        }
        return 'MyTone - ' + duration;
    }

    useEffect(() => {
        setMounted(true);
        return () => {
            setMounted(false);
        };
    }, [id]);

    function getTracks() {
        if (trackHolder.songs?.length > 50) {
            return trackHolder.songs?.slice(0, 50);
        }
        return trackHolder.songs;
    }

    const trackTable = (
        <div className="d-flex flex-column align-items-center justify-content-center">
            {isLoading ? (
                <Loader size="md"/>
            ) : (
                <>
                    <div className="d-flex w-100 justify-content-between">
                        <div className="d-flex mb-5">
                            <img id="trackHolderCover" height="232" width="232" src={trackHolder.image?.url}
                                 alt="albumCover"
                                 className="mr-5"/>
                            <div className="d-flex flex-column align-items-start justify-content-around">
                                <div className="d-flex flex-column align-items-start justify-content-center">
                                    <h1 className="display-4 font-weight-bold mb-2">{trackHolder.name}</h1>
                                    <div id="holderInfo" className="font-weight-bold">
                                        <div className="text-muted">
                                            {infoFirstRow}
                                        </div>
                                        <div className="text-muted">
                                            {infoSecondRow}
                                        </div>
                                    </div>
                                </div>
                                <div className="d-flex justify-content-center">
                                    <Link to="/signup" className="btn c-btn-main mr-2">
                                        <span className="px-3"><i className="fas fa-play"/> Play</span>
                                    </Link>
                                    <button type="button" className="btn c-btn-main-outline">
                                        <span className="px-3"><i className="fas fa-plus"/> Add to library</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="list-table-wrapper" className="p-1 w-100">
                        <Table className="media-table text-white" responsive>
                            <thead>
                            <tr>
                                <th>Tracks</th>
                            </tr>
                            </thead>
                            <tbody>
                            {getTracks()?.map((track, index) => (
                                <TrackItem key={uuid.v4()} index={index + 1} track={track}
                                           albumId={trackHolder.id} album={holderType === "album"}/>
                            ))}
                            </tbody>
                        </Table>
                    </div>
                </>)}
        </div>
    );

    return (
        <Container className="coming-soon-wrapper my-5">
            <Header className="header-wrapper" title="Album"/>
            {trackTable}
        </Container>
    );
}

const mapStateToProps = state => {
    return {
        track: state.player.track,
        playlist: state.player.playlist
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onPlayTrack: (player) => dispatch(actions.play(player))
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(TrackHolder)
);
