import React, {useEffect} from "react";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./App.css";
import Layout from "../components/layout/Layout";
import Home from "../components/pages/home/Home";
import NotFound from "../components/pages/not-found/NotFound";
import ConnectionErrorPage from "../components/connection-error-page/ConnectionErrorPage";
import * as actions from "../store/actions/index";
import axios from "axios";
import Album from "../components/track-holder/TrackHolder";
import Player from "../components/track/Track";
import SearchResults from "../components/pages/search-results/SearchResults";

function App(props) {
    useEffect(() => {
        props.onAutoSignup();
    }, []);

    axios.interceptors.response.use(
        (response) => {
            return response;
        },
        (error) => {
            console.log(error);
            if (!error.response) {
                props.history.push("/connectionError");
            }
            return Promise.reject(error);
        }
    );

    return (
        <div className="App">
            <Layout>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route
                        exact
                        path="/tracks/:type/:id"
                        component={Album}
                    />
                    <Route exact path="/player/:id" component={Player}/>
                    <Route
                        exact
                        path="/search/:input"
                        component={SearchResults}
                    />
                    <Route
                        exact
                        path="/connectionError"
                        component={ConnectionErrorPage}
                    />
                    <Route exact path="/pageNotFound" component={NotFound}/>
                    <Redirect from="*" to="/pageNotFound"/>
                </Switch>
            </Layout>
        </div>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAutoSignup: () => dispatch(actions.authCheckState()),
    };
};

export default withRouter(connect(null, mapDispatchToProps)(App));
