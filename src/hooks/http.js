import {useEffect, useState} from "react";
import axios from "axios";

export const useHttp = (url, dependencies) => {
    const [isLoading, setIsLoading] = useState(false);
    const [fetchedData, setFetchedData] = useState(null);

    useEffect(() => {
        fetchData(url);
    }, dependencies);

    const fetchData = async url => {
        try {
            setIsLoading(true);
            const response = await axios.get(url);
            setIsLoading(false);
            setFetchedData(response);
        } catch (error) {
            console.error(error);
            setIsLoading(false);
        }
    };
    return [isLoading, fetchedData];
};
