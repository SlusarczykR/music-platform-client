export const updateState = (oldState, updatedProps) => {
  return {
    ...oldState,
    ...updatedProps
  };
};
