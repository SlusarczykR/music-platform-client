import * as actionTypes from "./actionTypes";

export const modalShow = childId => {
  return {
    type: actionTypes.MODAL_SHOW,
    payload: {
      childId: childId
    }
  };
};

export const modalHide = () => {
  return {
    type: actionTypes.MODAL_HIDE
  };
};
