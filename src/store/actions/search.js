import * as actionTypes from "./actionTypes";
import axios from "axios";

export const search = (url, offset, items) => {
    return dispatch => {
        console.log("$ Search url: " + url + `/${offset}`);
        axios
            .get(url + `/${offset}`)
            .then(res => {
                console.log(res.data.length);
                dispatch(
                    searchSuccess({
                        hasMoreItems: res.data.length > 0,
                        items: items.concat(...res.data),
                        offset: offset + 20
                    })
                );
            })
            .catch(error => {
                dispatch(searchFail({error: error.message ? error.message : "An error has occurred"}));
            });
    };
};

export const initSearch = url => {
    console.log("$ Init url: " + url);
    return {
        type: actionTypes.INIT_SEARCH,
        url: url
    };
};

export const searchSuccess = data => {
    console.log(data);
    return {
        type: actionTypes.SEARCH_SUCCESS,
        payload: {
            hasMoreItems: data.hasMoreItems,
            items: data.items,
            offset: data.offset
        }
    };
};

export const searchFail = error => {
    return {
        type: actionTypes.SEARCH_FAIL,
        error: error
    };
};