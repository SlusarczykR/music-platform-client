import * as actionTypes from "./actionTypes";
import axios from "axios";
import oauth from "axios-oauth-client";
import {AUTH_CODE_URI, OAUTH_TOKEN} from "../../endpoints";

const getOwnerCredentials = (username, password) => {
    return oauth.client(
        axios.create({
            auth: {
                username: "musicPlatform",
                password: "secret"
            }
        }),
        {
            url: OAUTH_TOKEN,
            grant_type: "password",
            username: username,
            password: password
        }
    );
};

export const cleanupAuth = () => {
    return {
        type: actionTypes.CLEANUP_AUTH
    };
};

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = data => {
    console.log(data);
    return {
        type: actionTypes.AUTH_SUCCESS,
        payload: {
            token: data.value,
            username: data.username
        }
    };
};

export const authFail = error => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(authLogout());
        }, expirationTime * 1000);
    };
};

export const authLogout = error => {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    localStorage.removeItem("expiration");
    return {
        type: actionTypes.AUTH_LOGOUT,
        error: error
    };
};

export const auth = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        const fetchToken = getOwnerCredentials(username, password);
        fetchToken()
            .then(res => {
                const expirationDate = new Date(new Date().getTime() + res.expires_in * 1000);
                localStorage.setItem("token", res.access_token);
                localStorage.setItem("username", username);
                localStorage.setItem("expiration", expirationDate);
                dispatch(
                    authSuccess({
                        value: res.access_token,
                        username: username
                    })
                );
                dispatch(checkAuthTimeout(res.expires_in));

                return axios.get(AUTH_CODE_URI);
            })
            .catch(err => {
                dispatch(authFail(err.response ? err.response.data.error_description : "An error has occurred"));
            })
            .then(res => {
                window.location.replace(res.data);
            })
            .catch(err => {
                console.error(err);
            });
    };
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem("token");
        if (!token) {
            dispatch(authLogout());
        } else {
            const expiration = new Date(localStorage.getItem("expiration"));
            if (expiration <= new Date()) {
                dispatch(authLogout());
            } else {
                const username = localStorage.getItem("username");
                console.log(username);
                dispatch(authSuccess({value: token, username: username}));
                dispatch(
                    checkAuthTimeout((expiration.getTime() - new Date().getTime()) / 1000)
                );
            }
        }
    };
};
