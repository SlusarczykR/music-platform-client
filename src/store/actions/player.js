import * as actionTypes from "./actionTypes";

export const play = (player) => {
    return (dispatch) => {
        console.log("@@ Setting player track...");
        console.log(player);
        dispatch(setTrackData(player));
    };
};

export const setTrackData = (player) => {
    return {
        type: actionTypes.PLAY_TRACK,
        payload: {
            track: player.track,
            playlist: player.playlist
        }
    };
};

export const changeTrack = (track) => {
    return (dispatch) => {
        console.log("@@ Changing player track...");
        console.log(track);
        dispatch(setTrack(track));
    };
};

export const setTrack = (track) => {
    return {
        type: actionTypes.CHANGE_TRACK,
        payload: {
            track: track
        }
    };
};

export const changePlayingTrack = (track) => {
    return (dispatch) => {
        console.log("@@ Changing playing track...");
        console.log(track);
        dispatch(setPlayingTrack(track));
    };
};

export const setPlayingTrack = (track) => {
    return {
        type: actionTypes.CHANGE_PLAYING_TRACK,
        payload: {
            track: track
        }
    };
};