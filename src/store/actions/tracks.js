import * as actionTypes from "./actionTypes";
import axios from "axios";

export const fetchTracks = (url) => {
  return (dispatch) => {
    console.log("Fetching from: " + url);
    axios
      .get(url)
      .then((res) => {
        console.log(res.data);
        dispatch(
          fetchTracksSuccess({
            comments: res.data,
          })
        );
      })
      .catch((error) => {
        dispatch(
          fetchTracksFail({
            error: error.message ? error.message : "An error has occurred",
          })
        );
      });
  };
};

export const fetchTracksSuccess = (data) => {
  return {
    type: actionTypes.FETCH_TRACKS_SUCCESS,
    payload: {
      comments: data.comments,
    },
  };
};

export const fetchTracksFail = (error) => {
  return {
    type: actionTypes.FETCH_TRACKS_FAIL,
    error: error,
  };
};

export const addTrack = (url, body) => {
  return (dispatch) => {
    console.log("Post on: " + url);
    dispatch(addTrackStart());
    const token = localStorage.getItem("token");
    axios
      .post(url, body, {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "text/plain",
        },
      })
      .then((res) => {
        console.log(res.status);
        dispatch(
          addTrackSuccess({
            status: res.status,
          })
        );
      })
      .catch((error) => {
        dispatch(
          addTrackFail({
            error: error.message ? error.message : "An error has occurred",
          })
        );
      });
  };
};

export const addTrackStart = () => {
  return {
    type: actionTypes.ADD_TRACK_START,
  };
};

export const addTrackSuccess = (data) => {
  return {
    type: actionTypes.ADD_TRACK_SUCCESS,
    payload: {
      status: data.status,
    },
  };
};

export const addTrackFail = (error) => {
  return {
    type: actionTypes.ADD_TRACK_FAIL,
    error: error,
  };
};
