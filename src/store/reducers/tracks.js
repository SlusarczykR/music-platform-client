import * as actionTypes from "../actions/actionTypes";
import { updateState } from "../utility";

const initialState = {
  tracks: [],
  loading: false,
  status: null,
  error: null,
};

const fetchTracksSuccess = (state, action) => {
  return updateState(state, {
    tracks: action.payload.tracks,
    error: null,
  });
};

const fetchTracksFail = (state, action) => {
  return updateState(state, { error: action.error });
};

const addTrackStart = (state) => {
  return updateState(state, { error: null, loading: true });
};

const addTrackSuccess = (state, action) => {
  return updateState(state, {
    status: action.payload.status,
    loading: false,
    error: null,
  });
};

const addTrackFail = (state, action) => {
  return updateState(state, { loading: false, error: action.error });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_TRACKS_SUCCESS:
      return fetchTracksSuccess(state, action);
    case actionTypes.FETCH_TRACKS_FAIL:
      return fetchTracksFail(state, action);
    case actionTypes.ADD_TRACK_START:
      return addTrackStart(state, action);
    case actionTypes.ADD_TRACK_SUCCESS:
      return addTrackSuccess(state, action);
    case actionTypes.ADD_TRACK_FAIL:
      return addTrackFail(state, action);
    default:
      return state;
  }
};

export default reducer;
