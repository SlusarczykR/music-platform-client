import * as actionTypes from "../actions/actionTypes";
import {updateState} from "../utility";

const initialState = {
    url: "",
    items: [],
    hasMoreItems: false,
    offset: 0,
    error: null
};

const initSearch = (state, action) => {
    return updateState(state, {
        url: action.url,
        items: [],
        hasMoreItems: true,
        offset: 0,
        error: null
    });
};

const searchSuccess = (state, action) => {
    console.log(action.payload);
    return updateState(state, {
        hasMoreItems: action.payload.hasMoreItems && state.offset < 100,
        items: action.payload.items,
        offset: action.payload.offset, error: null
    });
};

const searchFail = (state, action) => {
    return updateState(state, {hasMoreItems: false, error: action.error});
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INIT_SEARCH:
            return initSearch(state, action);
        case actionTypes.SEARCH_SUCCESS:
            return searchSuccess(state, action);
        case actionTypes.SEARCH_FAIL:
            return searchFail(state, action);
        default:
            return state;
    }
};

export default reducer;
