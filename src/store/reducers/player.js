import * as actionTypes from "../actions/actionTypes";
import {updateState} from "../utility";

const initialState = {
    track: null,
    playingTrack: null,
    playlist: []
};

const setTrackData = (state, action) => {
    return updateState(state, {
        track: action.payload.track,
        playingTrack: action.payload.track,
        playlist: action.payload.playlist
    });
};

const setTrack = (state, action) => {
    return updateState(state, {
        track: action.payload.track,
    });
};

const setPlayingTrack = (state, action) => {
    return updateState(state, {
        playingTrack: action.payload.track,
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.PLAY_TRACK:
            return setTrackData(state, action);
        case actionTypes.CHANGE_TRACK:
            return setTrack(state, action);
        case actionTypes.CHANGE_PLAYING_TRACK:
            return setPlayingTrack(state, action);
        default:
            return state;
    }
};

export default reducer;
