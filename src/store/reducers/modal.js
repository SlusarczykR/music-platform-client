import * as actionTypes from "../actions/actionTypes";
import { updateState } from "../utility";

const initialState = {
  modalShow: false,
  childId: null
};

const showModal = (state, action) => {
  console.log(action.payload);
  return updateState(state, {
    modalShow: true,
    childId: action.payload.childId
  });
};

const hideModal = state => {
  return updateState(state, { modalShow: false });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.MODAL_SHOW:
      return showModal(state, action);
    case actionTypes.MODAL_HIDE:
      return hideModal(state);
    default:
      return state;
  }
};

export default reducer;
